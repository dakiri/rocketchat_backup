Ansible Playbook pour sauvegarde de la base données rocketchat et export via scp.

Application dans un environnement rocketchat tournant sous docker.

# Prérequis 

* Librairie python pour manipuler JSON 

```
pip install jmespath
```

* Clé ssh pour connexion au serveur de backup

Ex :
```
ssh-keygen -o -a 100 -t ed25519 -f id_backup
```

# Variables 

* backup_root

chemin mappé vers "dump" dans le container 

Ex :
```
    mongo:
         container_name: mongo
         image: mongo:4.0
[...]
         volumes:
           - /opt/docker/containers/redteams/data/db:/data/db
           - /opt/docker/containers/redteams/data/backups:/dump
```

* backup_account

compte sur le serveur de sauvegarde.

* destination_server

adresse du serveur de sauvegarde.

